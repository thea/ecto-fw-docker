FROM cern/cc7-base:20180516

ARG IPBB_VERSION

# RUN (for PACKAGE in \
#     git python-pip python-virtualenv gcc-c++ make which libX11 libXext libXft; \
#   do \
#     yum -y install ${PACKAGE} || exit $?; \
#   done) && \
#   yum clean all

RUN yum -y install \
    git python-pip python-virtualenv gcc-c++ make which libX11 libXext libXft && \
    yum clean all

RUN pip install --upgrade pip
RUN pip install https://github.com/ipbus/ipbb/archive/${IPBB_VERSION}.tar.gz